import numpy as np
import matplotlib.pyplot as plt
from Entregable2 import RKG,RKGA

# USO DE LA CLASE EN EL EJEMPLO PRACTICO

#Definimos la funcion objetivo para nuestro problema particular: dy/dx = x - y "
def funcion_derivada(x, y):
    return x-y

#Condiciones inciales del problema particular
x0=0
y0=0
xf=1

#Coeficientes asociadoes al metodo RK4
a= [[0, 0, 0, 0],[1/2, 0, 0, 0],[0, 1/2, 0, 0],[0, 0, 1, 0]]
b= [1/6, 1/3, 1/3, 1/6]
c= [0, 1/2, 1/2, 1]

#h=0.00001 #Paso
h=0.1 #Paso
h1=0.00001
h2=0.01
h3=0.001
h4=0.0001
rk = RKG(funcion_derivada, x0, y0, xf, h,a,b,c)
L=rk.soluciones()

IN=np.linspace(x0,xf,100001)
def Real(x):
  return x-1 + np.e**(-x)

IN1=np.linspace(x0,xf,100)
IN2=np.linspace(x0,xf,1000)
IN3=np.linspace(x0,xf,10001)
rk1 = RKG(funcion_derivada, x0, y0, xf, h1,a,b,c)
L1=rk1.soluciones()
rk2 = RKG(funcion_derivada, x0, y0, xf, h2,a,b,c)
L2=rk2.soluciones()
rk3 = RKG(funcion_derivada, x0, y0, xf, h3,a,b,c)
L3=rk3.soluciones()
rk4 = RKG(funcion_derivada, x0, y0, xf, h4,a,b,c)
L4=rk4.soluciones()



#Graficacion
Fig = plt.figure(figsize = (16,6))
plt.suptitle("Solucion a la ecuacion y´ + y = x , y(0)", fontsize=12)
plt.subplot(1,2,1)
plt.plot(L[0],L[1],".r", label="S Numerica")
plt.plot(IN,Real(IN),"--b", label="S Analitica")
plt.title("Solucion computacional")
plt.grid(c="black",lw=0.1)

plt.xlabel("$X$ ")
plt.ylabel(" $Y$ ")
plt.legend()

#MARCO
P = plt.gca()
C="purple"
P.spines['right'].set_color(C)
P.spines['top'].set_color(C)
P.spines['bottom'].set_color(C)
P.spines['left'].set_color(C)

plt.subplot(1,2,2)
plt.title("Convergencia de las soluciones")

plt.plot(L2[0],abs(L2[1]-Real(IN1)),"--b", label="h=0.01")
plt.plot(L3[0],abs(L3[1]-Real(IN2)),"--g", label="h=0.001")
plt.plot(L4[0],abs(L4[1]-Real(IN3)),"--",color="orange" ,label="h=0.0001")
plt.plot(L1[0],abs(L1[1]-Real(IN)),".r", label="h=0.00001")
plt.xscale('log')  # Escala logarítmica en el eje x para mejor visualización
plt.grid(c="black",lw=0.1)
plt.xlabel("$X$ ")
plt.ylabel(" $Y$ ")
plt.legend()

#MARCO
P = plt.gca()
C="purple"
P.spines['right'].set_color(C)
P.spines['top'].set_color(C)
P.spines['bottom'].set_color(C)
P.spines['left'].set_color(C)

plt.show()
